/*eslint-disable no-undef*/
const microsoftJwt = require('./index');

beforeAll(() => {
});

test('setup invalid parameters', () => {
    let invalidExpressParam = {},
        emptyOptionsParam = {},
        validExpressParam = {use: () => {}, get: () => {}},
        validJwtInvalidMicrosoftOptionsParam = {jwtAudience: 'aud', jwtIssuer: 'iss', jwtSecret: 'sec'};
    microsoftJwt.passport = {use: jest.fn(), initialize: jest.fn(() => {return {};})};
    expect(() => {microsoftJwt.setup(invalidExpressParam, emptyOptionsParam);}).toThrowError('Please supply a valid express instance to the setup function.');
    expect(() => {microsoftJwt.setup(validExpressParam, emptyOptionsParam);}).toThrowError('Options must contain values for jwtAudience, jwtIssuer and jwtSecret.');
    expect(() => {microsoftJwt.setup(validExpressParam, validJwtInvalidMicrosoftOptionsParam);}).toThrowError('Options must contain values for microsoftClientID, microsoftSecret and microsoftDomains.');
});

test('genToken', () => {
    expect(() => {
        return microsoftJwt.authenticationCallback({user: {
            id: 'cookies',
            displayName: 'cookiemonster',
            userPrincipalName: 'cookiemonster@sesamestreet.universe'
        }});
    });
});