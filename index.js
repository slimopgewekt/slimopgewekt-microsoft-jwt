/* eslint-disable no-console */
const passport = require('passport');
const microsoftStrategy = require('passport-microsoft').Strategy;
const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');

let isSetup = false;
let setupOptions = {};
const setup = (app, options) => {
    if(isSetup)
        return;
    if(!app.use || !app.get)
        throw new Error('Please supply a valid express instance to the setup function.');
    options = options || {};
    if(!options.jwtAudience || !options.jwtIssuer || !options.jwtSecret)
        throw new Error('Options must contain values for jwtAudience, jwtIssuer and jwtSecret.');
    if(!options.microsoftClientID || !options.microsoftSecret || !options.microsoftDomains || !options.microsoftCallbackUrl)
        throw new Error('Options must contain values for microsoftClientID, microsoftSecret and microsoftDomains.');
    setupOptions = options;
    passport.serializeUser((u, d) => d(null, u));
    passport.deserializeUser((u, d) => d(null, u));
    
    passport.use(new microsoftStrategy(
        {
            clientID: options.microsoftClientID,
            clientSecret: options.microsoftSecret,
            callbackURL: options.microsoftCallbackUrl,
            scope: 'https://graph.microsoft.com/user.read'
        },
        function(accessToken, refreshToken, profile, done) {
            return done(null, {...profile, accessToken});
        })
    );
    app.use(passport.initialize());
};
const authenticationHandler = passport.authenticate('microsoft');
const authenticationCallback = (req, res) => {
    const raw = req.user._json;
    if(!checkDomain(raw.userPrincipalName)){
        res.status(401);
        res.send({status: 'error', message: 'Invalid Domain'});
    }
    const token = genToken(
        { 
            microsoftId: req.user.id,
            name: req.user.displayName
        }, req.user.id);
    if(req.redirect){
        const uri = `${req.redirect}${req.redirect.indexOf('?') > -1 ? '&' : '?'}token=${token}`;
        res.redirect(uri);
    } else
        res.send({status: 'success', token});
}

const genToken = (payload, subject) => { 
    const token = jwt.sign(payload, setupOptions.jwtSecret, 
        {
            expiresIn: 86400, // expires in 24 hours
            issuer: setupOptions.jwtIssuer,
            audience: setupOptions.jwtAudience,
            subject: subject,
            encoding: 'utf8',
            keyid: subject,
            jwtid: uuidv4()
        }
    );
    return token;
};

const verifyToken = (token, cb) => {
    return jwt.verify(token, process.env.JWT_SECRET,{
        issuer: process.env.JWT_ISSUER,
        audience: process.env.JWT_AUDIENCE
    }, (error, decoded) => {       
        if(error)
            console.log(error); 
        cb(!error ? decoded : null);
    });
}

const verifyTokenHandler = (req, res, next) => {
    if(req.headers.authorization){
        const token = req.headers.authorization.replace('Bearer ', '');
        return verifyToken(token, decoded => {
            if (decoded) {
                req.user = decoded;
                next();
            } else {
                res.locals.setBody({error: 'Authorization failed', status: 401});
                next(new Error("Authorization failed"));
            }
        });
    }else{
        res.locals.setBody({error: 'Authorization failed', status: 401});
        return next(new Error("Authorization failed"));
    }
}

const checkDomain = (emailAddress) => {
    const validDomains = setupOptions.microsoftDomains.split(',') || [];
    return validDomains.reduce((agg, cur) => {
        return agg || emailAddress.indexOf(cur) > -1;
    }, false);
};

module.exports = {
    setup,
    authenticationHandler,
    authenticationCallback,
    verifyTokenHandler
};